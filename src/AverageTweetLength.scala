import java.util.concurrent.atomic.AtomicLong

import scala.io.Source

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter.TwitterUtils

object AverageTweetLength {
  def setupTwitter() {
    for (line <- Source.fromFile("data/twitter.txt").getLines()) {
      val fields = line.split(" ")
      System.setProperty("twitter4j.oauth." + fields(0), fields(1))
    }
  }
  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.ERROR)
    setupTwitter()
    val stc = new StreamingContext("local[*]", "TweetLength", Seconds(1))
    val tweets = TwitterUtils.createStream(stc, None);
    val statuses = tweets.map(status => status.getText())
    val lengths = statuses.map(status => status.length())

    var totalTweets = new AtomicLong(0)
    var totalCharacters = new AtomicLong(0)

    lengths.foreachRDD((rdd, _) => {
      var count = rdd.count()

      if (count > 0) {
        totalTweets.getAndAdd(count)
        totalCharacters.getAndAdd(rdd.reduce(_ + _))

        println("Total Tweets: " + totalTweets.get()
          + "\nTotal Characters: " + totalCharacters.get() +
          "\nAverage Length: " + Math.round(totalCharacters.get() / totalTweets.get().toDouble * 100.0)/100.0 + "\n")
      }
    })

    stc.start()
    stc.awaitTermination()
  }
}