import scala.io.Source

import org.apache.spark.streaming._
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.streaming.twitter.TwitterUtils

object PopularHashtags {
  def setupTwitter() = {
    for (line <- Source.fromFile("data/twitter.txt").getLines()) {
      val fields = line.split(" ")
      System.setProperty("twitter4j.oauth." + fields(0), fields(1))
    }
  }

  def main(args: Array[String]) {
    setupTwitter()
    Logger.getLogger("org").setLevel(Level.ERROR)
    val stc = new StreamingContext("local[*]", "PopularHashtags", Seconds(1))
    val tweets = TwitterUtils.createStream(stc, None)
    val statuses = tweets.map(_.getText())
    val bagOfWords = statuses.flatMap(_.split(" "))
    val hashtags = bagOfWords.filter(_.startsWith("#"))
    val hashtagKV = hashtags.map(tag => (tag, 1))
    val popularHashtags = hashtagKV.reduceByKeyAndWindow(_ + _, _ - _, Seconds(300), Seconds(10))
    val sortedResults = popularHashtags.transform(rdd => rdd.sortBy(x => x._2, false))

    /*
     * TwitterUtils.createStream(stc, None)
     *   .map(_.getText())
     *   .flatMap(_.split(" "))
     *   .filter(_.startsWith("#")
     *   .map(tag => (tag, 1))
     *   .reduceByKeyAndWindow(_ + _, _ - _, Seconds(300), Seconds(10))
     *   .transform(_.sortBy(x => x._2, false))
     *   .print
     */

    sortedResults.print
    stc.checkpoint(".")
    stc.start()
    stc.awaitTermination()
  }
}
