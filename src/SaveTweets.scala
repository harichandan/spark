import scala.io.Source

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.twitter.TwitterUtils

object SaveTweets {
  def setupTwitter() = {
    for (line <- Source.fromFile("data/twitter.txt").getLines()) {
      val fields = line.split(" ")
      System.setProperty("twitter4j.oauth." + fields(0), fields(1))
    }
  }

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.ERROR)
    setupTwitter()
    val stc = new StreamingContext("local[*]", "SaveTweets", Seconds(1))
    var tweetCount:Long = 0
    val tweets = TwitterUtils.createStream(stc, None)
    val statuses = tweets.map(status => status.getText())
    statuses.foreachRDD((rdd, time) => {
      val repartitionRDD = rdd.repartition(1).cache()
      repartitionRDD.saveAsTextFile("Tweets_" + time.milliseconds.toString)
      tweetCount += repartitionRDD.count()
      println("Tweet Count: " + tweetCount)
      if (tweetCount > 100) {
        System.exit(0)
      }
    })
    
    stc.start()
    stc.awaitTermination()
  }
}