import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.mllib.stat.Statistics
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.{Vector, Vectors}


object BasicStatistics {
  def main(arr: Array[String]) {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val conf = new SparkConf().setAppName("Stats").setMaster("local[*]")
    val sc = new SparkContext(conf);
    val seriesX: RDD[Double] = sc.parallelize(Array(1, 2, 3, 4, 5))
    val seriesY: RDD[Double] = sc.parallelize(Array(11, 22, 33, 44, 55))
    
    val correlation: Double = Statistics.corr(seriesX, seriesY, "pearson")
    println(s"Correlation is: $correlation")
    
    val data: RDD[Vector] = sc.parallelize(
      Seq(
        Vectors.dense(1.0, 10.0, 100.0),
        Vectors.dense(2.0, 20.0, 200.0),
        Vectors.dense(5.0, 33.0, 366.0)
      ) 
    )
    val correlMatrix = Statistics.corr(data, "pearson")
    println(correlMatrix)
  }
}