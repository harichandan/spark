import scala.math.random
import org.apache.spark._
import org.apache.log4j.Logger
import org.apache.log4j.Level

object SparkPi {
  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val conf = new SparkConf().setAppName("Spark Pi").setMaster("local[2]")
    val spark = new SparkContext(conf)
    
    val slices = 2
    val n = 10000000 * slices
    
    val count = spark.parallelize(1 to n, slices).map { i =>
      val x = random * 2 - 1
      val y = random * 2 - 1
      if (x*x + y*y < 1) 1 else 0
    }.reduce(_ + _)
    
    println("Pi is approximately " + 4*count.toFloat/n)
    spark.stop()
  }
}